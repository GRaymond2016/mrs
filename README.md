# README #


### What is this repository for? ###

This is an app to run on a small board like a cubieboard, it takes in data from sensors that are connected via USB for the most part and records them to a microSD. The aim was to have several streams of video, gps, gyro and accelerometer data. It is intended as a lap-timing software for motorbikes.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact