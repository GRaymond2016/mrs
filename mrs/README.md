# MRS
## External dependancies:
- Boost (1.5.5)
- libusb-1_0-devel
- Phidget 2.1
- Live 555
- pthread
- iw (Should be available by sudo apt-get install libiw-devel, wireless-tools-devel or similar)

Remember to rebuild indexes in eclipse following changes.

## Setup
Libaries should be in /usr/local/lib or /usr/lib. This software is not compatible with windows due to the makefile.

- Extract boost
- Copy src to /usr/src/boost - do not build or copy binaries here, unless you want to waste space.
- Build boost - Run ./bootstrap.sh and then just ./b2 stage to build.
- Copy libs from new stage directory to /usr/lib.
- Copy boost folder to /usr/src/boost_1.55.

- Extract live555 - contains 4 folders.
- Copy groupsock/include to /usr/local/include/groupsock/
- Repeat for other three folder in similar manner aka liveMedia/include to /usr/local/include/liveMedia. (BasicUsageEnvironment, UsageEnvironment are the other two)
- Build make files through genMakeFiles and then build the binaries.
- Copy binaries to /usr/local/lib

- Extract phidgets
- Copy c and h files to /usr/src/phidget
- Run make, and copy binary to /usr/local/lib
## Other Info 
- Talk to Greg Raymond for any information required.