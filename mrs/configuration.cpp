/*
 * configuration.cpp
 *
 *  Created on: 11/05/2014
 *      Author: lordnyson
 */
#include "configuration.hpp"

#include <iostream>
#include <fstream>
#include <map>

#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

configuration::configuration(const std::string& filename) : m_filename(filename)
{
}
void configuration::write_settings()
{
	std::ofstream stream(m_filename.c_str());
	boost::property_tree::xml_parser::write_xml(stream, ptree);
}
void configuration::read_settings()
{
	ptree = boost::property_tree::ptree();
	std::ifstream stream(m_filename.c_str());
	boost::property_tree::xml_parser::read_xml(stream, ptree);
}
const std::string configuration::operator[] (const std::string &name)
{
	return ptree.get_child("configuration").get_child("global").get<std::string>(name);
}

const boost::property_tree::ptree configuration::get_sub_node()
{
	return ptree.get_child("configuration");
}



