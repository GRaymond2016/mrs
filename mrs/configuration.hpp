#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

#include <string>
#include <boost/property_tree/ptree.hpp>

class configuration
{
public:
	configuration(const std::string& filename);
	void write_settings();
	void read_settings();
	const std::string operator[] (const std::string &name);
	const boost::property_tree::ptree get_sub_node();
private:
	std::string m_filename;
	boost::property_tree::ptree ptree;
};

#endif
