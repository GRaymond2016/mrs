/*
 * data_collector.cpp
 *
 *  Created on: 03/05/2014
 *      Author: lordnyson
 */
#include "data_collector.hpp"

#include "util.hpp"

data_collector::data_collector(unsigned tolerance_in_ms) : m_tolerance_in_ms(tolerance_in_ms), m_running(true)
{
	m_data_sink_lock.reset(new boost::mutex());
	m_cached_data_lock.reset(new boost::mutex());
	m_buffer_thread.reset(new boost::thread(boost::bind(cache_loop, this)));
}
data_collector::~data_collector()
{
	m_running = false;
	m_buffer_thread->join();
}
void data_collector::add_data(boost::shared_ptr<data_element> data)
{
	boost::mutex::scoped_lock lock_data(*m_cached_data_lock);
	m_cached_data[data->data_type].push_back(data);
}
void data_collector::add_data_sink(unsigned long long data_tag, boost::shared_ptr<data_sink> sink)
{
	boost::mutex::scoped_lock lock(*m_data_sink_lock);
	m_data_sinks.push_back(std::pair<unsigned long long, boost::shared_ptr<data_sink> >(data_tag, sink));
}
void data_collector::cache_loop(void * context)
{
	data_collector *collection = (data_collector*) context;
	while(collection->m_running)
	{
		boost::mutex::scoped_lock lock_sink(*collection->m_data_sink_lock);
		for(std::vector< std::pair <unsigned long long, boost::shared_ptr<data_sink> > >::iterator it = collection->m_data_sinks.begin();
				it != collection->m_data_sinks.end(); ++it)
		{
			boost::mutex::scoped_lock lock_data(*collection->m_cached_data_lock);
			for(std::vector< boost::shared_ptr<data_element> >::iterator it2 = collection->m_cached_data[it->first].begin();
					it2 != collection->m_cached_data[it->first].end();)
			{
				if (it->first == it2->get()->data_type)
					it->second->add_data(it2->get()->timestamp, it2->get()->data_type, it2->get()->data);
				else if (it2->get()->timestamp > (boost::uint64_t)(MRS::util::utctime_as_unixtime() - collection->m_tolerance_in_ms))
				{
					++it2;
					continue;
				}
				it2 = collection->m_cached_data[it->first].erase(it2);
			}
			it->second->flush();
		}
		lock_sink.unlock();
		boost::this_thread::sleep(boost::posix_time::milliseconds(collection->m_tolerance_in_ms));
	}
}
//Retrieval for currently cached data - revise this strategy
std::map<unsigned, std::string> data_collector::retrieve_data(unsigned long long timestamp)
{
	std::map<unsigned, std::string> data_closest_to_timestamp;
	boost::mutex::scoped_lock lock_data(*m_cached_data_lock);
	for(std::map< unsigned, std::vector< boost::shared_ptr<data_element> > >::iterator mp = m_cached_data.begin(); mp != m_cached_data.end(); ++mp)
	{
		for(std::vector< boost::shared_ptr<data_element> >::iterator it = mp->second.begin(); it != mp->second.end(); ++it)
		{
			if(it->get()->timestamp > timestamp - m_tolerance_in_ms)
			{
				data_closest_to_timestamp[mp->first] = std::string(&(*it->get()->data)[0], it->get()->data->size());
			}
		}
	}
	return data_closest_to_timestamp;
}
