/*
 * data_collector.hpp
 *
 *  Created on: 03/05/2014
 *      Author: lordnyson
 */

#ifndef DATA_COLLECTOR_HPP_
#define DATA_COLLECTOR_HPP_

#include <vector>
#include <map>
#include <string>

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>

#include "data_sink.hpp"
#include "data_source.hpp"

class data_collector
{
public:
	data_collector(unsigned tolerance_in_ms);
	~data_collector();
	void add_data(boost::shared_ptr<data_element>);
	void add_data_sink(unsigned long long data_tag, boost::shared_ptr<data_sink> sink);
	std::map<unsigned, std::string> retrieve_data(unsigned long long timestamp);

private:
	static void cache_loop(void * context);

	std::map< unsigned, std::vector< boost::shared_ptr<data_element> > > m_cached_data;
	std::vector< std::pair <unsigned long long, boost::shared_ptr<data_sink> > > m_data_sinks;
	unsigned m_tolerance_in_ms;

	boost::shared_ptr<boost::thread> m_buffer_thread;
	boost::shared_ptr<boost::mutex> m_data_sink_lock;
	boost::shared_ptr<boost::mutex> m_cached_data_lock;
	bool m_running;
};


#endif /* DATA_COLLECTOR_HPP_ */
