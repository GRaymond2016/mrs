/*
 * data_element.hpp
 *
 *  Created on: 11 Jul 2015
 *      Author: lordnyson
 */

#ifndef DATA_ELEMENT_HPP_
#define DATA_ELEMENT_HPP_

struct data_element
{
	data_element(const unsigned long long &data_type, boost::shared_ptr< std::vector<char> > data);
	unsigned unique_id;
	unsigned long long timestamp;
	unsigned long long data_type;
	boost::shared_ptr< std::vector<char> > data;
};

#endif /* DATA_ELEMENT_HPP_ */
