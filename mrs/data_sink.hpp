/*
 * data_sink.hpp
 *
 *  Created on: 03/06/2014
 *      Author: lordnyson
 */

#ifndef DATA_SINK_HPP_
#define DATA_SINK_HPP_

#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

class data_sink
{
public:
	virtual ~data_sink() {}
	virtual void add_data(unsigned long long timestamp, unsigned long long data_type, boost::shared_ptr< std::vector<char> > &data) = 0;
	virtual void flush() = 0;
	inline unsigned get_data_tag()
	{
		return data_tag;
	}

private:
	unsigned data_tag;
};


#endif /* DATA_SINK_HPP_ */
