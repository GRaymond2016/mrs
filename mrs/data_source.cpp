/*
 * data_source.cpp
 *
 *  Created on: 13/05/2014
 *      Author: lordnyson
 */
#include "data_source.hpp"

#include <boost/thread/lock_guard.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "util.hpp"
#include "data_types.hpp"

std::vector< boost::shared_ptr<data_element> > data_source::m_internal_map;
std::vector<std::string> data_source::m_data_sources;
boost::recursive_mutex data_source::m_internal_map_lock;

std::vector<data_element> data_source::get_data(unsigned long long timestamp, unsigned tolerance)
{
	return std::vector<data_element>();
}
void data_source::add_listener(boost::function<void(boost::shared_ptr<data_element>)> listener)
{
	m_listener = listener;
}
data_source::data_source()
{
	m_source_type = MRS::data_types::max_iden;
}
data_source::data_source(const unsigned long long& source)
{
	m_data_sources.push_back(MRS::data_types::name_from_data_type(source));
	m_source_type = source;
	m_listener = listen;
}
data_source::~data_source()
{

}
const unsigned long long data_source::source_type()
{
	return m_source_type;
}
void data_source::listen(boost::shared_ptr<data_element> el)
{
	boost::lock_guard<boost::recursive_mutex> lock(m_internal_map_lock);
	m_internal_map.push_back(el);
}
data_element::data_element(const unsigned long long &data_t, boost::shared_ptr< std::vector<char> > dat)
{
	static unsigned id = 0;
	timestamp = MRS::util::utctime_as_unixtime();
	unique_id = id++;
	data_type = data_t;
	data = dat;
}



