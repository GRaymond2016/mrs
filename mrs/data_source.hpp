#ifndef DATA_SOURCE_HPP
#define DATA_SOURCE_HPP

#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "data_element.hpp"

class data_source
{
public:
	data_source();
	virtual ~data_source();
	std::vector<data_element> get_data(unsigned long long timestamp, unsigned tolerance);
	void add_listener(boost::function<void(boost::shared_ptr<data_element>)> listener);

protected:
	data_source(const unsigned long long& source);
	boost::function<void(boost::shared_ptr<data_element>)> m_listener;
	const unsigned long long source_type();

private:
	static void listen(boost::shared_ptr<data_element>);
	static std::vector< boost::shared_ptr<data_element> > m_internal_map;
	static std::vector<std::string> m_data_sources;
	static boost::recursive_mutex m_internal_map_lock;

	unsigned long long m_source_type;
};

#endif
