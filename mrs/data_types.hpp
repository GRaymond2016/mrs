/*
 * data_types.hpp
 *
 *  Created on: 17/06/2014
 *      Author: lordnyson
 */

#ifndef DATA_TYPES_HPP_
#define DATA_TYPES_HPP_


namespace MRS {
namespace data_types {

	typedef unsigned data_type;

	const static unsigned unknown = -1;
	const static unsigned max_iden = 4000;
	const static unsigned h264_data = 0;
	const static unsigned audio_data = 3000;
	const static unsigned spatial_data = 1000;
	const static unsigned gps_data = 2000;

	inline unsigned inden_from_data_type(const unsigned &iden)
	{
		if (iden < 0)
			return unknown;
		if (iden < spatial_data)
			return h264_data;
		else if (iden >= spatial_data && iden < gps_data)
			return spatial_data;
		else if (iden >= gps_data && iden < audio_data)
			return gps_data;
		else if (iden < max_iden)
			return audio_data;
		return unknown;
	}

	inline std::string name_from_data_type(const unsigned &iden)
	{
		switch (inden_from_data_type(iden))
		{
			case h264_data: return "h264_data";
			case spatial_data: return "spatial_data";
			case gps_data: return "gps_data";
			case audio_data: return "audio_data";
			default: return "unknown_data_type";
		}
	}

	inline bool is_binary_data_type(const unsigned &iden)
	{
		switch (inden_from_data_type(iden))
		{
			case h264_data:
			case audio_data: return true;
			default: return false;
		}
	}
}
}


#endif /* DATA_TYPES_HPP_ */
