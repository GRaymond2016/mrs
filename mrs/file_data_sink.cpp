/*
 * file_data_sink.cpp
 *
 *  Created on: 17/06/2014
 *      Author: lordnyson
 */

#include "file_data_sink.hpp"

#include <sys/statvfs.h>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <boost/log/trivial.hpp>
#include <boost/date_time.hpp>

#include "util.hpp"
#include "data_types.hpp"

file_data_sink::file_data_sink(std::string path, std::string filename, std::string extension) :
m_path(path), m_filename(filename), m_extension(extension), m_last_file_start(0), m_last_data_type(0xFFFFFFFF)
{
}
void file_data_sink::add_data(unsigned long long timestamp, unsigned long long data_type, boost::shared_ptr< std::vector<char> > &data)
{
	assert(m_last_data_type == 0xFFFFFFFF || m_last_data_type == data_type || data_type >= 0);

	if (MRS::data_types::is_binary_data_type(data_type))
		m_buffer.write(&(*data)[0], data->size());
	else
	{
		m_buffer << timestamp << ",";
		m_buffer.write(&(*data)[0], data->size());
	}

	m_last_data_type = data_type;
}
void file_data_sink::cleanup()
{
	struct statvfs stats;
	if (!statvfs(m_path.c_str(), &stats))
	{
		if (stats.f_bfree < 500ULL * 1024ULL * 1024ULL * 1024ULL)
		{
			BOOST_LOG_TRIVIAL(warning) << "Free space has reduced below 500MB.";
		}
	}
}
void file_data_sink::flush()
{
	try
	{
		std::ofstream outputFile;
		if (m_last_file_start + MRS::util::time_frame::ONE_MINUTE * 5 < MRS::util::utctime_as_unixtime())
		{
			m_last_file_start = MRS::util::utctime_as_unixtime();
			std::stringstream newFileName;
			newFileName << m_path << MRS::util::formatted_utctime() << "_" << m_filename << "." << m_extension;
			m_current_file = newFileName.str();
		}

		if(m_current_file.size() > 0) {
			outputFile.open(m_current_file.c_str(), std::ios_base::app);
		}

		if (!outputFile.is_open())
		{
			BOOST_LOG_TRIVIAL(error) << "Could not open file " << m_current_file << ".";
			m_buffer.str(std::string());
			m_buffer.clear();
			return;
		}
		outputFile << m_buffer.str();
		m_buffer.str(std::string());
		m_buffer.clear();
		outputFile.close();
	}
	catch (...)
	{
		BOOST_LOG_TRIVIAL(error) << "Failed flushing to disk.";
	}
}


