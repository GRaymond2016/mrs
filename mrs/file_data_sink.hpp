/*
 * file_data_sink.hpp
 *
 *  Created on: 17/06/2014
 *      Author: lordnyson
 */

#ifndef FILE_DATA_SINK_HPP_
#define FILE_DATA_SINK_HPP_

#include <sstream>
#include <boost/cstdint.hpp>
#include "data_sink.hpp"

class file_data_sink : public data_sink
{
public:
	file_data_sink(std::string path, std::string filename, std::string extension);
	virtual void add_data(unsigned long long timestamp, unsigned long long data_type, boost::shared_ptr< std::vector<char> > &data);
	virtual void flush();

private:
	void cleanup();
	std::stringstream m_buffer;
	std::string m_path;
	std::string m_filename;
	std::string m_extension;

	boost::int64_t m_last_file_start;
	std::string m_current_file;

	boost::uint64_t m_last_data_type;
};


#endif /* FILE_DATA_SINK_HPP_ */
