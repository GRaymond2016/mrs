/*
 * geo_maths.hpp
 *
 *  Created on: 2 Jul 2015
 *      Author: lordnyson
 */

#ifndef SRC_GEO_MATHS_HPP_
#define SRC_GEO_MATHS_HPP_

#include <boost/math/constants/constants.hpp>

struct geo_maths
{
	inline static double to_radians(double deg)
	{
		return deg * boost::math::constants::pi<double>() / 180.0;
	}
	/*
	 * Returns the greater circle distance in meters.
	 */
	static double measure_distance(double lat1, double lon1, double lat2, double lon2)
	{
		const double R = 6371;
		double latDiff = to_radians(lat2 - lat1);
		double lonDiff = to_radians(lon2 - lon1);

		double a = sin(latDiff / 2) * sin(latDiff / 2) +
					cos(to_radians(lat1) / 2) * cos(to_radians(lat2) / 2) +
					sin(lonDiff / 2) * sin(lonDiff / 2);
		return R * (2 * atan2(sqrt(a), sqrt(1-a)));
	}
};


#endif /* SRC_GEO_MATHS_HPP_ */
