/*
 * gps_data_proxy.cpp
 *
 *  Created on: 9 May 2015
 *      Author: lordnyson
 */

#ifndef GPS_DATA_PROXY_CPP_
#define GPS_DATA_PROXY_CPP_

#include "gps_data_proxy.hpp"

#include <stdio.h>
#include <sstream>
#include <iomanip>

#include <boost/log/trivial.hpp>

#include "data_types.hpp"
#include "gps_timestamp.hpp"

gps_data_proxy::gps_data_proxy(int offset) : data_source(MRS::data_types::gps_data + offset), gps(CPhidgetGPSHandle())
{
	CPhidget_enableLogging(PHIDGET_LOG_DEBUG, NULL);
	CPhidgetGPS_create(&gps);

	CPhidget_set_OnAttach_Handler((CPhidgetHandle) gps, attached_handler, this);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle) gps, detached_handler, this);
	CPhidget_set_OnError_Handler((CPhidgetHandle) gps, error_handler, this);
}
gps_data_proxy::~gps_data_proxy()
{
	CPhidget_close((CPhidgetHandle) gps);
	CPhidget_delete((CPhidgetHandle) gps);
}

void gps_data_proxy::init()
{
	CPhidgetGPS_set_OnPositionChange_Handler(gps, gps_handler, this);
	CPhidgetGPS_set_OnPositionFixStatusChange_Handler(gps, fix_handler, this);

	CPhidget_open((CPhidgetHandle) gps, -1);
	BOOST_LOG_TRIVIAL(info) << "Initialized gps library, waiting for data..";
}

int CCONV gps_data_proxy::gps_handler(CPhidgetGPSHandle handle, void *context, double lat, double lon, double alt)
{
	gps_data_proxy* data_proxy = (gps_data_proxy*) context;
	GPSDate date;
	GPSTime time;
	double heading, velocity;

	CPhidgetGPS_getDate(data_proxy->gps, &date);
	CPhidgetGPS_getTime(data_proxy->gps, &time);
	CPhidgetGPS_getHeading(data_proxy->gps, &heading);
	CPhidgetGPS_getVelocity(data_proxy->gps, &velocity);

	std::stringstream ss;
	gps_timestamp timestamp(date.tm_year, date.tm_mon, date.tm_mday,
							time.tm_hour, time.tm_min, time.tm_sec, time.tm_ms);
	ss << timestamp << ",";
	ss << lat << "," << lon << "," << alt << "," << heading << "," << velocity;
	ss << std::endl << "\0";

	boost::shared_ptr< std::vector<char> > data_internal(new std::vector<char>(ss.str().size()));
	memcpy(&(*data_internal)[0], &ss.str()[0], ss.str().size());
	boost::shared_ptr<data_element> data_el(new data_element(data_proxy->source_type(), data_internal));

	data_proxy->m_listener(data_el);
	return 0;
}

int CCONV gps_data_proxy::fix_handler(CPhidgetGPSHandle handle, void *, int)
{
	return 0;
}

int CCONV gps_data_proxy::attached_handler(CPhidgetHandle gps, void *)
{
	return 0;
}
int CCONV gps_data_proxy::detached_handler(CPhidgetHandle gps, void *)
{
	return 0;
}
int CCONV gps_data_proxy::error_handler(CPhidgetHandle gps, void *, int, const char*)
{
	return 0;
}

#endif /* GPS_DATA_PROXY_CPP_ */
