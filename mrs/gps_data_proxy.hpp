/*
 * gps_data_proxy.hpp
 *
 *  Created on: 9 May 2015
 *      Author: lordnyson
 */

#ifndef GPS_DATA_PROXY_HPP_
#define GPS_DATA_PROXY_HPP_

#include <phidget21.h>
#include "data_source.hpp"

class gps_data_proxy : public data_source
{
public:
	gps_data_proxy(int offset);
	~gps_data_proxy();
	void init();

private:
	CPhidgetGPSHandle gps;

	static int CCONV gps_handler(CPhidgetGPSHandle handle, void *context, double, double, double);
	static int CCONV fix_handler(CPhidgetGPSHandle handle, void *context, int);

	static int CCONV attached_handler(CPhidgetHandle gps, void * context);
	static int CCONV detached_handler(CPhidgetHandle gps, void *);
	static int CCONV error_handler(CPhidgetHandle gps, void *, int, const char*);
};

#endif /* GPS_DATA_PROXY_HPP_ */
