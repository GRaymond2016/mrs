/*
 * gps_timestamp.hpp
 *
 *  Created on: 30 Jun 2015
 *      Author: lordnyson
 */

#ifndef GPS_TIMESTAMP_HPP_
#define GPS_TIMESTAMP_HPP_

#include <ostream>
#include <istream>
#include <boost/cstdint.hpp>
#include <boost/date_time/posix_time/ptime.hpp>

#include "util.hpp"

struct gps_timestamp
{
	short year;
	short month;
	short day;
	short hour;
	short minute;
	short second;
	short millisecond;

	gps_timestamp() : year(0), month(0), day(0), hour(0), minute(0), second(0), millisecond(0)
	{}

	gps_timestamp(short year, short month, short day, short hour, short minute, short second, short millisecond) :
		year(year), month(month), day(day), hour(hour), minute(minute), second(second), millisecond(millisecond)
	{}

	boost::uint64_t to_timestamp()
	{
		return MRS::util::ptime_to_unixtime(
		boost::posix_time::ptime(boost::gregorian::date(year, month, day),
							boost::posix_time::time_duration(hour, minute, second, millisecond)));
	}

	friend std::ostream& operator<<(std::ostream &out, gps_timestamp const &data)
	{
		out << data.day << "-" << data.month << "-" << data.year << ",";
		out << data.hour << ":" << data.minute << ":" << data.second << "." << data.millisecond;
		return out;
	}

	friend std::istream& operator>>(std::istream &out, gps_timestamp &data)
	{
		char interleave;
		out >> data.day;
		out >> interleave;
		out >> data.month;
		out >> interleave;
		out >> data.year;
		out >> interleave;

		out >> data.hour;
		out >> interleave;
		out >> data.minute;
		out >> interleave;
		out >> data.second;
		out >> interleave;
		out >> data.millisecond;
		return out;
	}
};

#endif /* GPS_TIMESTAMP_HPP_ */
