/*
 * lap_time_data_analyser.cpp
 *
 *  Created on: 30 Jun 2015
 *      Author: lordnyson
 */
#include "lap_time_data_analyser.hpp"

#include <sstream>
#include <boost/log/trivial.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>

#include "geo_maths.hpp"
#include "gps_timestamp.hpp"
#include "data_types.hpp"

std::ostream& operator<<(std::ostream &out, const geopoint &data)
{
	out << data.time << "," << data.lat << "," << data.lon << "," << data.altitude << ",";
	out << data.heading << "," << data.velocity;
	return out;
}

std::istream& operator>>(std::istream &out, geopoint &data)
{
	char interleave;
	out >> data.time;
	out >> interleave;
	out >> data.lat;
	out >> interleave;
	out >> data.lon;
	out >> interleave;
	out >> data.altitude;
	out >> interleave;
	out >> data.heading;
	out >> interleave;
	out >> data.velocity;
	return out;
}

lap_time_data_analyser::lap_time_data_analyser(double max_dist) : laps(), m_running(false), m_max_dist(max_dist)
{

}
void lap_time_data_analyser::add_data(unsigned long long timestamp, unsigned long long data_type, boost::shared_ptr< std::vector<char> > &data)
{
	std::stringstream stream;
	stream.write(&(*data)[0], data->size());
	switch(MRS::data_types::inden_from_data_type(data_type))
	{
		case MRS::data_types::gps_data:
			process_gps(stream);
			break;
		case MRS::data_types::spatial_data:
			break;
		default:
			BOOST_LOG_TRIVIAL(warning) << "Received unknown data type in lap_time_data_analyser: " << data_type;
			break;
	}
}
void lap_time_data_analyser::process_gps(std::stringstream &stream)
{
	geopoint location;
	gps_timestamp gps_time;
	stream >> gps_time;
	location.time = gps_time.to_timestamp();
	char inter;
	stream >> inter;
	stream >> location.lat;
	stream >> inter;
	stream >> location.lon;
	stream >> inter;
	stream >> location.altitude;
	stream >> inter;
	stream >> location.heading;
	stream >> inter;
	stream >> location.velocity;

	boost::lock_guard<boost::mutex> lock(current_lap_mutex);
	current_lap.push_back(location);
}
void lap_time_data_analyser::flush()
{
	//Do nothing
}
void lap_time_data_analyser::process_loop(void* context)
{
	lap_time_data_analyser* this_ptr = (lap_time_data_analyser*) context;
	lap first_lap;
	while(this_ptr->m_running)
	{
		boost::lock_guard<boost::mutex> lock(this_ptr->current_lap_mutex);
		if (this_ptr->current_lap.size() > 1)
		{
			sleep(32);
			continue;
		}
		geopoint last = this_ptr->current_lap[this_ptr->current_lap.size()-1];
		for (unsigned i = 0; i < this_ptr->current_lap.size() - 1; ++i)
		{
			double dist = geo_maths::measure_distance(last.lat, last.lon, this_ptr->current_lap[i].lat,  this_ptr->current_lap[i].lon);
			if (dist < this_ptr->m_max_dist)
			{
				this_ptr->laps.push_back(this_ptr->current_lap);
				this_ptr->m_listener(MRS::util::format_data_array<geopoint>(this_ptr->source_type(), this_ptr->current_lap));
				this_ptr->current_lap.clear();
				if (first_lap.size() == 0)
				{
					std::copy(this_ptr->current_lap.begin(), this_ptr->current_lap.end(), std::back_inserter(first_lap));
					//TODO: Find somewhere in this lap to mark as the finish line and use it as the last point of the "lap" from then on.
				}
			}
		}
	}
}



