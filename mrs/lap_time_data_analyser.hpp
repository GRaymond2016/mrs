#ifndef LAP_TIME_DATA_ANALYSER_H
#define LAP_TIME_DATA_ANALYSER_H

#include <iostream>
#include <sstream>

#include <boost/thread/mutex.hpp>

#include "data_sink.hpp"
#include "data_source.hpp"

struct sector
{
	double lat;
	double lon;
};

struct geopoint
{
	unsigned long long time;
	double lat;
	double lon;
	double altitude;
	double heading;
	double velocity;
};

std::ostream& operator<<(std::ostream &out, const geopoint &data);
std::istream& operator>>(std::istream &out, geopoint &data);

typedef std::vector<geopoint> lap;

class lap_time_data_analyser : public data_sink, public data_source
{
public:
	lap_time_data_analyser(double);
	void add_data(unsigned long long timestamp, unsigned long long data_type, boost::shared_ptr< std::vector<char> > &data);
	void flush();

private:
	static void process_loop(void*);
	void process_gps(std::stringstream &stream);

	boost::mutex current_lap_mutex;
	lap current_lap;
	std::vector<lap> laps;
	std::vector<sector> sectors;

	bool m_running;
	double m_max_dist;
};

#endif
