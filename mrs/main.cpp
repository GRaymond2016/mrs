/*
 * main.cpp
 *
 *  Created on: 03/05/2014
 *      Author: lordnyson
 */
#include <boost/filesystem.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup.hpp>
#include <boost/exception/all.hpp>
#include <signal.h>

#include "configuration.hpp"
#include "source_wrapper.hpp"

void setup_logging(std::string path)
{
	boost::filesystem::create_directories(path);

	boost::log::add_console_log (
		std::cout,
		boost::log::keywords::format = "[%TimeStamp%]: %Message%",
	    boost::log::keywords::auto_flush = true
	);
	boost::log::add_file_log (
	    boost::log::keywords::file_name = path + "MRSLog_%3N.log",
	    boost::log::keywords::rotation_size = 1 * 1024 * 1024,
	    boost::log::keywords::max_size = 20 * 1024 * 1024,
	    boost::log::keywords::time_based_rotation = boost::log::sinks::file::rotation_at_time_point(0, 0, 0),
	    boost::log::keywords::format = "[%TimeStamp%]: %Message%",
	    boost::log::keywords::auto_flush = true
	  );

	boost::log::add_common_attributes();
}

void signal_callback(int sig)
{
	exit(sig);
}

int main(int argc, char** argv)
{
	signal(SIGINT, signal_callback);
	BOOST_LOG_TRIVIAL(info) << "Reading configuration.";
	configuration config("configuration.xml");
	config.read_settings();

	std::string outputpath = boost::any_cast<std::string>(config["outputpath"]);
	setup_logging(outputpath);

	source_wrapper launcher = source_wrapper(std::string(argv[0]));
	launcher.init(config);

	while (true)
		sleep(1);

	BOOST_LOG_TRIVIAL(info) << "Quitting gracefully";
	return 0;
}

