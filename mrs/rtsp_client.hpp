/*
 * rtsp_client.hpp
 *
 *  Created on: 03/05/2014
 *      Author: lordnyson
 */

#ifndef RTSP_CLIENT_HPP_
#define RTSP_CLIENT_HPP_

#include <string>
#include <BasicUsageEnvironment.hh>
#include <UsageEnvironment.hh>
#include <NetAddress.hh>
#include <RTSPClient.hh>
#include <liveMedia.hh>

#include "rtsp_client_state.hpp"

class rtsp_client : public RTSPClient
{
public:
	static rtsp_client* create(MRS::data_types::data_type source, UsageEnvironment &env, const std::string &rtspURL, int verbosity_lvl,
			const std::string &app_name, portNumBits http_tunnel_num)
	{
		return new rtsp_client(source, env, rtspURL, verbosity_lvl, app_name, http_tunnel_num);
	}
	static void cleanup(rtsp_client* client)
	{
		delete client;
	}
	rtsp_client_state& get_scs()
	{
		return m_scs;
	}
	unsigned get_transport()
	{
		return m_transport;
	}
	MRS::data_types::data_type source()
	{
		return m_source;
	}

protected:
	rtsp_client(MRS::data_types::data_type source, UsageEnvironment &env, const std::string &rtspURL, int verbosity_lvl, const std::string &app_name, portNumBits http_tunnel_num) :
		RTSPClient(env, rtspURL.c_str(), verbosity_lvl, app_name.c_str(), http_tunnel_num, -1), m_transport(0), m_source(source)
	{}
	virtual ~rtsp_client()
	{}

private:
	MRS::data_types::data_type m_source;
	rtsp_client_state m_scs;
	unsigned m_transport;
};


#endif /* RTSP_CLIENT_HPP_ */
