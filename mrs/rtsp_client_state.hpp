#ifndef RTSP_CLIENT_STATE
#define RTSP_CLIENT_STATE

#include <liveMedia.hh>
#include <BasicUsageEnvironment.hh>

class rtsp_client_state {
public:
	rtsp_client_state() : iter(NULL), session(NULL), subsession(NULL), streamTimerTask(NULL), duration(0.0)
	{}
	virtual ~rtsp_client_state()
	{
		delete iter;
		if(session != NULL)
		{
			UsageEnvironment& env = session->envir();
			env.taskScheduler().unscheduleDelayedTask(streamTimerTask);
			Medium::close(session);
		}
	}
public:
	MediaSubsessionIterator* iter;
	MediaSession* session;
	MediaSubsession* subsession;
	TaskToken streamTimerTask;
	double duration;
};

#endif
