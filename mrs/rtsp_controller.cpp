/*
 * rtsp_controller.cpp
 *
 *  Created on: 03/05/2014
 *      Author: lordnyson
 */
#include "rtsp_controller.hpp"

#include <boost/log/trivial.hpp>
#include <boost/log/support/date_time.hpp>

#include <MediaSession.hh>
#include <RTCP.hh>

#include "rtsp_client.hpp"
#include "rtsp_client_state.hpp"
#include "rtsp_data_proxy.hpp"

std::vector< boost::function<void(data_source &)> > rtsp_controller::m_listeners;

rtsp_controller::rtsp_controller() :
		m_scheduler(NULL),
		m_environment(NULL),
		m_event_loop_watch(0),
		m_setup_complete(false)
{
}
rtsp_controller::~rtsp_controller()
{
	if (!m_event_loop_watch)
		teardown();
}
void rtsp_controller::setup()
{
	m_setup_complete = true;
	m_scheduler = BasicTaskScheduler::createNew();
	m_environment = BasicUsageEnvironment::createNew(*m_scheduler);
	m_event_loop_watch = 0;
}
void rtsp_controller::do_event_loop()
{
	m_scheduler->doEventLoop(&m_event_loop_watch);
}
void rtsp_controller::open_url(MRS::data_types::data_type source, std::string& app_name, std::string& rtsp_url, unsigned http_tunnel_port)
{
	rtsp_client* cl = rtsp_client::create(source, *m_environment, rtsp_url, 0, app_name, http_tunnel_port);
	m_current_clients.push_back(cl);
	cl->sendDescribeCommand(post_describe);
}
void rtsp_controller::add_data_source_listener(boost::function<void(data_source &)> handler)
{
	if (!m_setup_complete)
	{
		m_listeners.push_back(handler);
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << "Cannot add data source listener after setup.";
	}
}
std::vector<data_element> rtsp_controller::get_data(unsigned long long timestamp, unsigned tolerance)
{
	BOOST_LOG_TRIVIAL(error) << "You should not be calling get_data() for an rtsp_controller";
	return std::vector<data_element>();
}
void rtsp_controller::add_listener(boost::function<void(boost::shared_ptr<data_element>)> listener)
{
	BOOST_LOG_TRIVIAL(error) << "You should not be calling add_listener() for an rtsp_controller";
}
void rtsp_controller::teardown()
{
	for (unsigned i = 0; i < m_current_clients.size(); ++i)
	{
		this->close_handle(m_current_clients[i]);
	}
	m_setup_complete = false;
	m_event_loop_watch = 1;
	if(m_environment != NULL)
		m_environment->reclaim();
	if(m_scheduler != NULL)
		delete m_scheduler;
}
void rtsp_controller::post_describe(RTSPClient* rtsp_handle, int res_c, char* res_s)
{
	if(res_c != 0)
	{
		BOOST_LOG_TRIVIAL(warning) << "Failed to get a SDP description from stream: " << res_s << ".";
		delete[] res_s;
		reset_client(rtsp_handle);
		return;
	}
	UsageEnvironment& env = rtsp_handle->envir();
	rtsp_client_state& scs = ((rtsp_client*) rtsp_handle)->get_scs();

	char* const sdp = res_s;
	scs.session = MediaSession::createNew(env, sdp);
	delete[] res_s;

	if(scs.session == NULL || !scs.session->hasSubsessions())
	{
		BOOST_LOG_TRIVIAL(warning) << "Invalid SDP description or the session has no media subsessions.";
		reset_client(rtsp_handle);
		return;
	}

	scs.iter = new MediaSubsessionIterator(*scs.session);
	next_subsession(rtsp_handle);
}
void rtsp_controller::next_subsession(RTSPClient* rtsp_handle)
{
	UsageEnvironment& env = rtsp_handle->envir();
	rtsp_client_state& scs = ((rtsp_client*) rtsp_handle)->get_scs();

	while (NULL != (scs.subsession = scs.iter->next()))
	{
		if(!scs.subsession->initiate())
		{
			BOOST_LOG_TRIVIAL(debug) << "Failed to initialize subsession: " << env.getResultMsg() << ".";
			next_subsession(rtsp_handle);
		}
		else
		{
			rtsp_handle->sendSetupCommand(*scs.subsession, post_setup);
		}
		return;
	}

	scs.duration = scs.session->playEndTime() - scs.session->playStartTime();
	rtsp_handle->sendPlayCommand(*scs.session, post_play);
}
void rtsp_controller::pack_header(MediaSubsession* subsession, rtsp_data_proxy* proxy)
{
	std::vector<char> start_code = proxy->get_start_code();
	unsigned sc_size = start_code.size();
	if (strcmp(subsession->codecName(), "H265"))
	{
		unsigned spropRecs;
		SPropRecord* sPropRecords = parseSPropParameterSets(subsession->fmtp_spropparametersets(), spropRecs);
		std::vector<char> buffer;
		int buffPos = 0;
		for (unsigned i = 0; i < spropRecs; ++i)
		{
			buffer.resize(buffer.size() + sc_size + sPropRecords[i].sPropLength);
			memcpy(&buffer[buffPos], &start_code[0], sc_size);
			buffPos += sc_size;
			memcpy(&buffer[buffPos], sPropRecords[i].sPropBytes, sPropRecords[i].sPropLength);
			buffPos += sPropRecords[i].sPropLength;
		}
		delete [] sPropRecords;
		proxy->set_sprop(buffer);
	}
	else if (strcmp(subsession->codecName(), "H265"))
	{
		std::vector<char> buffer;
		const char * headers[] = { subsession->fmtp_spropvps(),
									subsession->fmtp_spropsps(),
									subsession->fmtp_sproppps() };
		for (unsigned j = 0; j < 3; ++j)
		{
			unsigned spropRecs;
			SPropRecord* sPropRecords = parseSPropParameterSets(headers[j], spropRecs);
			int buffPos = 0;
			for (unsigned i = 0; i < spropRecs; ++i)
			{
				buffer.resize(buffer.size() + sc_size + sPropRecords[i].sPropLength);
				memcpy(&buffer[buffPos], &start_code[0], sc_size);
				buffPos += sc_size;
				memcpy(&buffer[buffPos], sPropRecords[i].sPropBytes, sPropRecords[i].sPropLength);
				buffPos += sPropRecords[i].sPropLength;
			}

			delete [] sPropRecords;
		}
		proxy->set_sprop(buffer);
	}
}
void rtsp_controller::post_setup(RTSPClient* rtsp_handle, int result, char* result_str)
{
	UsageEnvironment& env = rtsp_handle->envir();
	rtsp_client* client = (rtsp_client*) rtsp_handle;
	rtsp_client_state& scs = client->get_scs();
	delete [] result_str;

	if(!result)
	{
		rtsp_data_proxy *proxy = new rtsp_data_proxy(client->source(), env, *scs.subsession, rtsp_handle->url());
		scs.subsession->sink = proxy;

		for (unsigned i = 0; i < m_listeners.size(); ++i)
		{
			m_listeners[i](*proxy);
		}

		scs.subsession->miscPtr = rtsp_handle;
		pack_header(scs.subsession, proxy);
		scs.subsession->sink->startPlaying(*(scs.subsession->readSource()), subsession_destruction, scs.subsession);
		if(scs.subsession->rtcpInstance() != NULL)
		{
			RTCPInstance* instance = scs.subsession->rtcpInstance();
			instance->setByeHandler(subsession_destruction, scs.subsession);
		}
		BOOST_LOG_TRIVIAL(info) << "Starting playback of URL:" << rtsp_handle->url() << ", Stream:" << scs.subsession->mediumName() << ", Codec:" << scs.subsession->codecName();
	}
	else
	{
		BOOST_LOG_TRIVIAL(warning) << "Failed to begin playing of subsession. " << env.getResultMsg() << ".";
	}

	next_subsession(rtsp_handle);
}
void rtsp_controller::post_play(RTSPClient* rtsp_handle, int result, char* result_str)
{
	delete [] result_str;
	//May require a sleep here or a pause till end of stream. This will hang a thread per stream.
	if(!result)
	{
		BOOST_LOG_TRIVIAL(info) << "Stream has successfully received a PLAY message";
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << "Closing stream due to an unrecoverable error.";
		close_handle(rtsp_handle);
	}
}
void rtsp_controller::reset_client(RTSPClient* rtsp_client)
{
	BOOST_LOG_TRIVIAL(info) << "Resetting stream: " << rtsp_client->url();
	sleep(1);
	rtsp_client->sendDescribeCommand(post_describe);
}
void rtsp_controller::subsession_destruction(void* client_data)
{
	MediaSubsession* subsession = (MediaSubsession*) client_data;
	RTSPClient* rtsp_client = (RTSPClient*) subsession->miscPtr;

	Medium::close(subsession->sink);
	subsession->sink = NULL;

	MediaSession& sess = subsession->parentSession();
	MediaSubsessionIterator iter(sess);
	while ((subsession = iter.next()) != NULL)
	{
		if(subsession->sink != NULL) return;
	}
	reset_client(rtsp_client);
}
void rtsp_controller::close_handle(RTSPClient* rtsp_handle)
{
	rtsp_client_state& scs = ((rtsp_client*) rtsp_handle)->get_scs();

	if(scs.session != NULL)
	{
		bool need_cleanup = false;
		MediaSubsessionIterator iter(*scs.session);
		MediaSubsession* subsession;

		while((subsession = iter.next()) != NULL)
		{
			if(subsession->sink != NULL)
			{
				Medium::close(subsession->sink);
				subsession->sink = NULL;

				if(subsession->rtcpInstance() != NULL)
				{
					RTCPInstance* instance = subsession->rtcpInstance();
					instance->setByeHandler(NULL, NULL);
				}
				need_cleanup = true;
			}
		}

		if(need_cleanup)
			rtsp_handle->sendTeardownCommand(*scs.session, NULL);
	}

	BOOST_LOG_TRIVIAL(info) << "Shutting down stream.";
	Medium::close(rtsp_handle);
}
