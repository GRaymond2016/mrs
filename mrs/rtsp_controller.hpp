/*
 * rtsp_controller.hpp
 *
 *  Created on: 03/05/2014
 *      Author: lordnyson
 */

#ifndef RTSP_CONTROLLER_HPP_
#define RTSP_CONTROLLER_HPP_

#include <vector>
#include <string>

#include <UsageEnvironment.hh>
#include <RTSPClient.hh>

#include <boost/function.hpp>

#include "rtsp_data_proxy.hpp"
#include "data_source.hpp"

class rtsp_controller : public data_source
{
public:
	rtsp_controller();
	~rtsp_controller();
	void setup();
	void do_event_loop();
	void open_url(MRS::data_types::data_type source, std::string& app_name, std::string& rtsp_url, unsigned http_tunnel_port);
	void add_data_source_listener(boost::function<void(data_source &)>);
	std::vector<data_element> get_data(unsigned long long timestamp, unsigned tolerance);
	void add_listener(boost::function<void(boost::shared_ptr<data_element>)> listener);
	void teardown();

private:
	static void post_describe(RTSPClient* rtsp_handle, int res_c, char* res_s);
	static void next_subsession(RTSPClient* rtsp_handle);
	static void post_setup(RTSPClient* rtsp_handle, int result, char* result_str);
	static void post_play(RTSPClient* rtsp_handle, int result, char* result_str);
	static void subsession_destruction(void* client_data);
	static void close_handle(RTSPClient* rtsp_handle);

	static void reset_client(RTSPClient* rtsp_handle);

	static void pack_header(MediaSubsession* subsession, rtsp_data_proxy* proxy);

	static std::vector< boost::function<void(data_source &)> > m_listeners;
	bool m_setup_complete;

	std::vector<RTSPClient*> m_current_clients;
	TaskScheduler* m_scheduler;
	UsageEnvironment* m_environment;
	char m_event_loop_watch;
};


#endif /* RTSP_CONTROLLER_HPP_ */
