/*
 * rtsp_data_proxy.cpp
 *
 *  Created on: 03/05/2014
 *      Author: lordnyson
 */
#include "rtsp_data_proxy.hpp"

#include <boost/log/trivial.hpp>
#include <boost/log/support/date_time.hpp>

#include <MediaSink.hh>

rtsp_data_proxy::rtsp_data_proxy(MRS::data_types::data_type offset, UsageEnvironment& env, MediaSubsession& subsession, std::string streamID)
: MediaSink(env),
  data_source(get_type_from_medium_type(offset, subsession.mediumName())),
  m_stream_id(streamID),
  m_subsession(subsession),
  m_recv_buffer(new u_int8_t[rtsp_data_proxy::recv_buffer_size]),
  m_start_code(std::vector<char>(4)),
  m_propsWritten(false)
{
	m_start_code[3] = 0x01;
}
rtsp_data_proxy::~rtsp_data_proxy()
{
}
MRS::data_types::data_type rtsp_data_proxy::get_type_from_medium_type(MRS::data_types::data_type offset, std::string medium)
{
	if (medium.compare("video") == 0)
		return MRS::data_types::h264_data + offset;
	else if (medium.compare("audio") == 0)
		return MRS::data_types::audio_data + offset;
	BOOST_LOG_TRIVIAL(error) << "Medium type did not match an MRS data type: " << medium;
	return MRS::data_types::max_iden;
}
void rtsp_data_proxy::set_sprop(std::vector<char> str)
{
	m_sprop = str;
}
std::vector<char> rtsp_data_proxy::get_start_code()
{
	return m_start_code;
}
void rtsp_data_proxy::post_frame(void* context, unsigned frame_size, unsigned trunc_bytes, struct timeval pres_time, unsigned dur_in_ms)
{
	rtsp_data_proxy* cont = (rtsp_data_proxy*) context;
	unsigned long long src = cont->source_type();
	int writeLoc = 0;
	boost::shared_ptr< std::vector<char> > data;
	if (src == MRS::data_types::h264_data)
	{
		if (!cont->m_propsWritten)
		{
			data = boost::shared_ptr< std::vector<char> >(new std::vector<char>(frame_size + cont->m_sprop.size() + cont->m_start_code.size()));
			cont->m_propsWritten = true;
			memcpy(&(*data)[writeLoc], &cont->m_sprop[0], cont->m_sprop.size());
			writeLoc += cont->m_sprop.size();
		}
		else
			data = boost::shared_ptr< std::vector<char> >(new std::vector<char>(frame_size + cont->m_start_code.size()));

		memcpy(&(*data)[writeLoc], &cont->m_start_code[0], cont->m_start_code.size());
		writeLoc += cont->m_start_code.size();
	}
	else
		data = boost::shared_ptr< std::vector<char> >(new std::vector<char>(frame_size));
	memcpy(&(*data)[writeLoc], &cont->m_recv_buffer[0], frame_size);

	boost::shared_ptr<data_element> element(new data_element(src, data));
	try
	{
		cont->m_listener(element);
	}
	catch(...)
	{
		BOOST_LOG_TRIVIAL(error) << "An exception occurred passing data from rtsp_data_proxy to listener.";
	}

	cont->continuePlaying();
}
Boolean rtsp_data_proxy::continuePlaying()
{
	if(fSource == NULL) return false;

	fSource->getNextFrame((unsigned char*) m_recv_buffer.get(), rtsp_data_proxy::recv_buffer_size,
							post_frame, this, onSourceClosure, this);
	return true;
}




