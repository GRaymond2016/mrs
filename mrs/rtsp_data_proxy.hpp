/*
 * rtsp_data_proxy.hpp
 *
 *  Created on: 03/05/2014
 *      Author: lordnyson
 */

#ifndef RTSP_DATA_PROXY_HPP_
#define RTSP_DATA_PROXY_HPP_

#include <string>
#include <vector>
#include <boost/scoped_array.hpp>
#include <liveMedia.hh>

#include "data_source.hpp"
#include "data_types.hpp"

class rtsp_data_proxy : public MediaSink, public data_source
{
public:
	rtsp_data_proxy(MRS::data_types::data_type offset, UsageEnvironment& env, MediaSubsession& subsession, std::string streamID);
	void set_sprop(std::vector<char>);
	std::vector<char> get_start_code();

	const static unsigned recv_buffer_size = 131072;
private:
	virtual ~rtsp_data_proxy();
	virtual Boolean continuePlaying();

	MRS::data_types::data_type get_type_from_medium_type(MRS::data_types::data_type offset, std::string medium);
	static void post_frame(void* context, unsigned frame_size, unsigned trunc_bytes, struct timeval pres_time, unsigned dur_in_ms);

	boost::scoped_array<u_int8_t> m_recv_buffer;
	std::vector<char> m_start_code;
	MediaSubsession& m_subsession;
	std::string m_stream_id;
	std::vector<char> m_sprop;
	bool m_propsWritten;
};


#endif /* RTSP_DATA_PROXY_HPP_ */
