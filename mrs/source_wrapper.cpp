/*
 *  source_wrapper.cpp
 *
 *  Created on: 12 Apr 2015
 *      Author: lordnyson */
#include "source_wrapper.hpp"
#include <boost/log/trivial.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>
#include <boost/thread/thread.hpp>

#include "data_types.hpp"
#include "spatial_data_proxy.hpp"
#include "gps_data_proxy.hpp"
#include "file_data_sink.hpp"
#include "usb_video_capture.hpp"

source_wrapper::source_wrapper(std::string program) :
	controller(20000),
	program_name(program)
{

}
void source_wrapper::add_data_source(data_source &source)
{
	source.add_listener(boost::bind(&data_collector::add_data, boost::ref(controller), _1));
}
std::string source_wrapper::get_connection()
{
	if (connections.size() == 0)
		return "";
	std::string retVal = connections[connections.size()-1];
	connections.erase(--connections.end());
	return retVal;
}
void source_wrapper::init(configuration &config)
{
	std::string outputpath = config["outputpath"];
	BOOST_LOG_TRIVIAL(info) << "Output path configured as: " << outputpath;

	int spatial_sources = 0, camera_sources = 0, gps_sources = 0, camera_index = 0;
	bool controller_setup = false;
	BOOST_LOG_TRIVIAL(info) << "Setting up collectors and data sinks.";

	try
	{
		boost::property_tree::ptree tree = config.get_sub_node();
		for(boost::property_tree::ptree::const_iterator v = tree.begin(); v != tree.end(); ++v)
		{
			std::string first = v->first;
			boost::property_tree::ptree second = v->second;
			std::string type, spec, name;
			if (first.compare("source") || !second.count("<xmlattr>"))
				continue;
			BOOST_FOREACH(boost::property_tree::ptree::value_type const& attr, second.get_child("<xmlattr>"))
			{
				if (attr.first.compare("type") == 0)
					type = attr.second.data();
				if (attr.first.compare("spec") == 0)
					spec = attr.second.data();
				if (attr.first.compare("name") == 0)
					name = attr.second.data();
			}
			if (type.compare("webcam") == 0)
			{
				BOOST_LOG_TRIVIAL(info) << "Starting webcam source <" << name << ">";
				boost::shared_ptr<usb_video_capture> capture(new usb_video_capture(camera_sources));
				capture->add_listener(boost::bind(&data_collector::add_data, boost::ref(controller), _1));
				controller.add_data_sink(MRS::data_types::h264_data + camera_sources,
																boost::shared_ptr<data_sink>(new file_data_sink(outputpath, "videodata-" + name, "raw")));
				capture->init(camera_index + camera_sources++);
				sources.push_back(capture);
			}
			else
			if (type.compare("rtsp") == 0)
			{
				BOOST_LOG_TRIVIAL(info) << "Starting rtsp source <" << name << ">";
				if (!controller_setup)
				{
					cam_controller.add_data_source_listener(boost::bind(&source_wrapper::add_data_source, this, _1));
					cam_controller.setup();
					controller_setup = true;
				}
				std::string url;
				for(boost::property_tree::ptree::const_iterator it = second.begin(); it != second.end(); it++) {
					if (it->first.compare("rtspurl") == 0)
						url = it->second.data();
				}
				if (!url.size())
				{
					BOOST_LOG_TRIVIAL(warning) << "Could not retrieve URL for an RTSP source.";
					continue;
				}
				if (spec.compare("gopro") == 0)
				{
					std::string conn = get_connection();
					if (!conn.size())
					{
						BOOST_LOG_TRIVIAL(error) << "No connection to bind source <" << name << "> to";
						continue;
					}
					wifi_configurer configurer(conn,
							boost::any_cast<std::string>(second.get_child("wifi-name").data()),
							boost::any_cast<std::string>(second.get_child("wifi-password").data()));
					configurer.setup_adapter();
					configurations.push_back(configurer);
				}
				try
				{
					BOOST_LOG_TRIVIAL(info) << "Setting up feed <" << url << ">";
					controller.add_data_sink(MRS::data_types::h264_data + camera_sources,
												boost::shared_ptr<data_sink>(new file_data_sink(outputpath, "h264data-" + name, "h264")));
					controller.add_data_sink(MRS::data_types::audio_data + camera_sources,
												boost::shared_ptr<data_sink>(new file_data_sink(outputpath, "audiodata-" + name, "aud")));
					cam_controller.open_url(camera_sources++, program_name, url, 0);
				}
				catch(const std::exception& e)
				{
					BOOST_LOG_TRIVIAL(error) << "Caught std exception: " << e.what();
				}
				catch(...)
				{
					BOOST_LOG_TRIVIAL(error) << "Uncaught exception was discarded; continuing execution.";
				}
			}
			else if (type.compare("spatial") == 0)
			{
				BOOST_LOG_TRIVIAL(info) << "Starting spatial source <" << name << ">";
				boost::shared_ptr<spatial_data_proxy> proxy(new spatial_data_proxy(spatial_sources));
				proxy->add_listener(boost::bind(&data_collector::add_data, boost::ref(controller), _1));
				proxy->init(16);
				controller.add_data_sink(MRS::data_types::spatial_data + spatial_sources++,
						boost::shared_ptr<data_sink>(new file_data_sink(outputpath, "spatial-" + name, "txt")));
				sources.push_back(proxy);
			}
			else if (type.compare("gps") == 0)
			{
				BOOST_LOG_TRIVIAL(info) << "Starting gps source <" << name << ">";
				boost::shared_ptr<gps_data_proxy> proxy(new gps_data_proxy(gps_sources));
				proxy->add_listener(boost::bind(&data_collector::add_data, boost::ref(controller), _1));
				proxy->init();
				controller.add_data_sink(MRS::data_types::gps_data + gps_sources++,
						boost::shared_ptr<data_sink>(new file_data_sink(outputpath, "gps-" + name, "txt")));
				sources.push_back(proxy);
			}
			else
			{
				BOOST_LOG_TRIVIAL(info) << "Failed to setup source <" << name << "> of type <" << type << ">";
			}
		}

		if (controller_setup)
		{
			BOOST_LOG_TRIVIAL(info) << "Launching polling threads.";
			boost::thread cam(boost::bind(&rtsp_controller::do_event_loop, boost::ref(cam_controller)));
		}
	}
	catch(std::exception const& ex)
	{
		BOOST_LOG_TRIVIAL(error) << "A fatal error occurred reading configurations.";
		BOOST_LOG_TRIVIAL(error) << ex.what();
	}
}
