#ifndef SOURCE_WRAPPER_HPP
#define SOURCE_WRAPPER_HPP

#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

#include "configuration.hpp"
#include "wifi_configurer.hpp"
#include "data_collector.hpp"
#include "rtsp_controller.hpp"

class source_wrapper
{
public:
	source_wrapper(std::string program);
	void init(configuration &config);
private:
	std::string get_connection();
	void add_data_source(data_source &source);

	std::string program_name;

	std::vector< boost::shared_ptr<data_source> > sources;
	std::vector<std::string> connections;
	std::vector<wifi_configurer> configurations;

	data_collector controller;
	rtsp_controller cam_controller;
};

#endif
