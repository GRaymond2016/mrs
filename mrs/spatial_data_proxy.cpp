/*
 * spatial_data_proxy.cpp
 *
 *  Created on: 13/05/2014
 *      Author: lordnyson
 */
#include "spatial_data_proxy.hpp"

#include <stdio.h>

#include <boost/log/trivial.hpp>
#include <boost/log/support/date_time.hpp>

#include "data_types.hpp"
#include "util.hpp"

spatial_data_proxy::spatial_data_proxy(int offset) : data_source(MRS::data_types::spatial_data + offset)
{
	CPhidget_enableLogging(PHIDGET_LOG_DEBUG, NULL);

	dev_handle = 0;
	CPhidgetSpatial_create(&dev_handle);
	CPhidget_set_OnAttach_Handler((CPhidgetHandle) dev_handle, attached_handler, this);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle) dev_handle, detached_handler, this);
	CPhidget_set_OnError_Handler((CPhidgetHandle) dev_handle, error_handler, this);
}
spatial_data_proxy::~spatial_data_proxy()
{
	CPhidget_close((CPhidgetHandle) dev_handle);
	CPhidget_delete((CPhidgetHandle) dev_handle);
}
void spatial_data_proxy::init(unsigned data_r)
{
	CPhidgetSpatial_set_OnSpatialData_Handler(dev_handle, data_handler, this);
	CPhidget_open((CPhidgetHandle) dev_handle, -1);

	data_rate = data_r;
	BOOST_LOG_TRIVIAL(info) << "Initialized spatial library, waiting for data..";
}
void spatial_data_proxy::startup_watcher(unsigned max_timeout)
{
	const unsigned ten_secs = 10000;
	unsigned iter = 0;
	int result = 0;
	while((result = CPhidget_waitForAttachment((CPhidgetHandle) dev_handle, ten_secs))
			&& max_timeout > ten_secs * iter)
	{
		const char * err;
		CPhidget_getErrorDescription(result, &err);
		BOOST_LOG_TRIVIAL(debug) << "Spatial device has not started yet. Current error: " << err;
		iter++;
	}
}
int CCONV spatial_data_proxy::data_handler(CPhidgetSpatialHandle spatial, void *context, CPhidgetSpatial_SpatialEventDataHandle *data, int count)
{
	spatial_data_proxy* handle = (spatial_data_proxy*) context;

	int data_rate;
	CPhidgetSpatial_getDataRate(spatial, &data_rate);
	data_rate *= count;
	//double accelx = 0, accely = 0, accelz = 0;
	//double angx = 0, angy = 0, angz = 0;
	//double dirx = 0, diry = 0, dirz = 0;
	for(int i = 0; i < count; ++i)
	{
		struct axis_data {
			double data [3];
		};

		//accelx += data[i]->acceleration[0];
		//accely += data[i]->acceleration[1];
		//accelz += data[i]->acceleration[2];

		//angx += data[i]->angularRate[0];
		//angy += data[i]->angularRate[1];
		//angz += data[i]->angularRate[2];

		//dirx += data[i]->magneticField[0];
		//diry += data[i]->magneticField[1];
		//dirz += data[i]->magneticField[2];

		std::stringstream ss;

		ss << data[i]->acceleration[0] << "," << data[i]->acceleration[1] << "," << data[i]->acceleration[2] << ",";
		ss << data[i]->angularRate[0] << "," << data[i]->angularRate[1] << "," << data[i]->angularRate[2] << ",";
		ss << data[i]->magneticField[0] << "," << data[i]->magneticField[1] << "," << data[i]->magneticField[2] << ",";
		ss << std::endl << "\0";

		boost::shared_ptr< std::vector<char> > data_internal(new std::vector<char>(ss.str().size()));
		memcpy(&(*data_internal)[0], &ss.str()[0], ss.str().size());
		boost::shared_ptr<data_element> data_el(new data_element(handle->source_type(), data_internal));

		handle->m_listener(data_el);
	}

	//handle->current_velocity += accelx;
	//handle->current_angle += angy;

	return 0;
}
int CCONV spatial_data_proxy::attached_handler(CPhidgetHandle spatial, void * context)
{
	spatial_data_proxy* handle = (spatial_data_proxy*) context;
	CPhidgetSpatial_setDataRate(handle->dev_handle, handle->data_rate);
	return 0;
}
int CCONV spatial_data_proxy::detached_handler(CPhidgetHandle spatial, void *)
{
	return 0;
}
int CCONV spatial_data_proxy::error_handler(CPhidgetHandle spatial, void *, int, const char*)
{
	return 0;
}



