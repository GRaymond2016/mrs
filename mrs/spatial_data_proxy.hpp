/*
 * accelerometer_data_proxy.hpp
 *
 *  Created on: 13/05/2014
 *      Author: lordnyson
 */

#ifndef ACCELEROMETER_DATA_PROXY_HPP_
#define ACCELEROMETER_DATA_PROXY_HPP_

#include <phidget21.h>

#include "data_source.hpp"

class spatial_data_proxy : public data_source
{
public:
	spatial_data_proxy(int offset);
	void init(unsigned data_rate);
	void startup_watcher(unsigned max_timeout);
	~spatial_data_proxy();

private:
	static int CCONV data_handler(CPhidgetSpatialHandle spatial, void *userptr, CPhidgetSpatial_SpatialEventDataHandle *data, int count);
	static int CCONV attached_handler(CPhidgetHandle spatial, void *);
	static int CCONV detached_handler(CPhidgetHandle spatial, void *);
	static int CCONV error_handler(CPhidgetHandle spatial, void *, int, const char*);

	CPhidgetSpatialHandle dev_handle;
	unsigned long long samples;
	unsigned data_rate;

	double current_velocity;
	double current_angle;
	double current_direction;
};


#endif /* ACCELEROMETER_DATA_PROXY_HPP_ */
