/*
 * usb_video_capture.cpp
 *
 *  Created on: 1 Jun 2015
 *      Author: lordnyson
 */
#include "usb_video_capture.hpp"
#include "data_types.hpp"

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include <linux/videodev2.h>

#include <boost/log/trivial.hpp>


usb_video_capture::usb_video_capture(int offset) :
data_source(offset + MRS::data_types::h264_data),
m_cam_offset(0),
m_active(true),
m_width(1680),
m_height(720)
{
}

usb_video_capture::~usb_video_capture()
{
	m_active = false;
	launch.timed_join(boost::posix_time::milliseconds(150));
	launch.interrupt();
}

void usb_video_capture::init(int cam_offset)
{
	m_cam_offset = cam_offset;
	m_active = true;
	launch = boost::thread(boost::bind(&usb_video_capture::poll, this));
}

int usb_video_capture::block_ioctl(int device, int request, void* args)
{
	int result;
	do
	{
		result = ioctl(device, request, args);
	} while (-1 == result && EINTR == errno);
	return result;
}

void usb_video_capture::poll(void * context)
{
	usb_video_capture* this_object = (usb_video_capture* ) context;
	int device =-1;
	void* mem_buffer;
	BOOST_LOG_TRIVIAL(info) << "USB Camera polling thread active.";
	while(this_object->m_active)
	{
		std::stringstream dev;
		dev << "/dev/video" << this_object->m_cam_offset;
		std::string device_name = dev.str();
		if (device == -1 && (device = open(device_name.c_str(), O_RDWR)) == -1)
		{
			BOOST_LOG_TRIVIAL(error) << "Could not open usb camera device <" + device_name + ">.";
			this_object->m_cam_offset++;
			if (this_object->m_cam_offset > 20)
				this_object->m_cam_offset = 0;
			continue;
		}
		BOOST_LOG_TRIVIAL(info) << "USB Camera <" + device_name + "> opened.";

		struct v4l2_capability caps = {0};
		if (-1 == block_ioctl(device, VIDIOC_QUERYCAP, &caps))
		{
			BOOST_LOG_TRIVIAL(error) << "Could not query usb camera device <" + device_name + "> capabilities.";
			continue;
		}

		struct v4l2_format fmt = {0};
		fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		fmt.fmt.pix.width = this_object->m_width;
		fmt.fmt.pix.height = this_object->m_height;
		fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
		fmt.fmt.pix.field = V4L2_FIELD_NONE;
		if (-1 == block_ioctl(device, VIDIOC_S_FMT, &fmt))
		{
			BOOST_LOG_TRIVIAL(error) << "Pixel format could not be set for usb camera device <" + device_name + ">, have you misconfigured?";
			continue;
		}

		struct v4l2_requestbuffers req = {0};
		req.count = 1;
		req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		req.memory = V4L2_MEMORY_MMAP;
		if (-1 == block_ioctl(device, VIDIOC_REQBUFS, &req))
		{
			BOOST_LOG_TRIVIAL(warning) << "Could not request buffer from usb camera device <" + device_name + ">.";
			continue;
		}
		BOOST_LOG_TRIVIAL(info) << "USB Camera <" + device_name + "> initialised.";

		struct v4l2_buffer buffer = {0};
		buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buffer.memory = V4L2_MEMORY_MMAP;
		buffer.index = 0;
		mem_buffer = mmap(NULL, buffer.length, PROT_READ | PROT_WRITE, MAP_SHARED, device, buffer.m.offset);
		if (-1 == block_ioctl(device, VIDIOC_QUERYBUF, &buffer))
		{
			BOOST_LOG_TRIVIAL(warning) << "Could not query buffer from the usb camerea device <" + device_name + ">.";
			continue;
		}
		if (-1 == block_ioctl(device, VIDIOC_STREAMON, &buffer.type))
		{
			BOOST_LOG_TRIVIAL(warning) << "Could not initialise playback on usb camera device <" + device_name + ">.";
			continue;
		}
		BOOST_LOG_TRIVIAL(info) << "USB Camera <" + device_name + "> stream up.";
		while (this_object->m_active)
		{
			fd_set fds;
			FD_ZERO(&fds);
			FD_SET(device, &fds);
			struct timeval time = {0};
			time.tv_sec = 5;
			int result = select(*reinterpret_cast<int*>(device + 1), &fds, NULL, NULL, &time);
			if (-1 == result)
			{
				BOOST_LOG_TRIVIAL(debug) << "Timed out waiting for frame from usb camera device <" + device_name + ">.";
				break;
			}
			if (-1 == block_ioctl(device, VIDIOC_DQBUF, &buffer))
			{
				BOOST_LOG_TRIVIAL(info) << "Failed retrieving frame from usb camera device <" + device_name + ">.";
				break;
			}

			boost::shared_ptr< std::vector<char> > data_internal(new std::vector<char>(buffer.bytesused));
			memcpy(&(*data_internal)[0], mem_buffer, buffer.bytesused);
			boost::shared_ptr<data_element> data_el(new data_element(this_object->source_type(), data_internal));

			this_object->m_listener(data_el);
		}
	}
}



