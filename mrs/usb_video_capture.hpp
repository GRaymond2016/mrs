/*
 * usb_video_capture.hpp
 *
 *  Created on: 1 Jun 2015
 *      Author: lordnyson
 */

#ifndef USB_VIDEO_CAPTURE_HPP_
#define USB_VIDEO_CAPTURE_HPP_

#include <boost/thread/thread.hpp>

#include "data_source.hpp"

class usb_video_capture : public data_source
{
public:
	usb_video_capture(int offset);
	~usb_video_capture();

	void init(int cam_offset);

private:
	static void poll(void *);
	static int block_ioctl(int device, int request, void* args);

	boost::thread launch;
	int m_cam_offset;
	int m_height;
	int m_width;
	bool m_active;
};


#endif /* USB_VIDEO_CAPTURE_HPP_ */
