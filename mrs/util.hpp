/*
 * util.hpp
 *
 *  Created on: 03/06/2014
 *      Author: lordnyson
 */

#ifndef UTIL_HPP_
#define UTIL_HPP_

#include <boost/date_time/local_time_adjustor.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>
#include <sstream>
#include <string>

#include "lap_time_data_analyser.hpp"
#include "data_element.hpp"

namespace MRS {
namespace util {

namespace time_frame {
	static const boost::int64_t ONE_MINUTE = boost::posix_time::minutes(1).total_milliseconds();
};

inline boost::int64_t ptime_to_unixtime(boost::posix_time::ptime const &input)
{
	boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
	return (input - epoch).total_milliseconds();
}

inline boost::int64_t localtime_as_unixtime()
{
	boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
	return (boost::posix_time::microsec_clock::local_time() - epoch).total_milliseconds();
}

inline boost::int64_t utctime_as_unixtime()
{
	boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
	return (boost::posix_time::ptime(boost::gregorian::day_clock::universal_day(), boost::posix_time::microsec_clock::universal_time().time_of_day()) - epoch).total_milliseconds();
}

inline std::string formatted_utctime()
{
	boost::posix_time::ptime tm(boost::gregorian::day_clock::universal_day(), boost::posix_time::microsec_clock::universal_time().time_of_day());
	//This is freed in the string stream destruction.
	boost::posix_time::time_facet* facet = new boost::posix_time::time_facet("%Y%m%d_%H%M%S");

	std::stringstream ss;
	ss.exceptions(std::ios_base::failbit);
	ss.imbue(std::locale(ss.getloc(), facet));
	ss << boost::posix_time::ptime(tm);
	return ss.str();
}

template<typename data>
inline boost::shared_ptr<data_element> format_data(unsigned long long src, data dta)
{
	boost::shared_ptr< std::vector<char> > data_internal(new std::vector<char>(dta.size()));
	memcpy(&(*data_internal)[0], &dta[0], dta.size());
	return boost::shared_ptr<data_element>(new data_element(src, data_internal));
}

template<typename data>
boost::shared_ptr<data_element> format_data_array(unsigned long long src, std::vector<data> dt)
{
	std::stringstream ss;
	for (int i = 0; i < dt.size(); ++i)
	{
		ss << dt[i];
	}
	return format_data<std::string>(src, ss.str());
}

}}

#endif /* UTIL_HPP_ */
