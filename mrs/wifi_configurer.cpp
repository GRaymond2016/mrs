/*
 * wifi_configurer.cpp
 *
 *  Created on: 11/11/2014
 *      Author: nyson
 */

#include "wifi_configurer.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/log/trivial.hpp>
#include <boost/thread/thread.hpp>

const static char * network_config = "/etc/network/interfaces";

wifi_configurer::wifi_configurer(std::string wireless_adapter, std::string wireless_network, std::string wireless_password) :
		continue_running(true),
		wireless_adapter(wireless_adapter),
		wireless_network(wireless_network),
		wireless_password(wireless_password)
{
}
void wifi_configurer::search()
{
	wireless_scan_head head;
	wireless_scan* result;
	iwrange range;
	int socket;

	socket = iw_sockets_open();

	if (iw_get_range_info(socket, wireless_adapter.c_str(), & range) < 0)
	{
		BOOST_LOG_TRIVIAL(error) << "Error getting range info on wifi network (" + wireless_adapter + "). Unable to search for networks.";
		return;
	}

	char buffer[64];
	memcpy(buffer, wireless_adapter.c_str(), std::min(64u, (unsigned) wireless_adapter.size()));
	if (iw_scan(socket, buffer, (int) range.we_version_compiled, &head) < 0)
	{
		BOOST_LOG_TRIVIAL(error) << "Failed to scan for available networks (" + wireless_adapter + "). Unable to search for networks.";
		return;
	}

	result = head.result;
	while (NULL != result)
	{
		BOOST_LOG_TRIVIAL(debug) << "Found Network: " + std::string(result->b.essid);
		result = result->next;
		if (wireless_network.compare(&result->b.essid[0]) == 0)
		{
			current_address = *result;
		}
	}
}
void wifi_configurer::poll_connect()
{
	continue_running = true;
	boost::thread polling_thread(boost::bind(&wifi_configurer::continuous_scan, this));
}
std::string wifi_configurer::remove_interface_entry(std::istream &input)
{
	char buffer [1024] = { 0 };
	input >> buffer;
	std::string to_parse(buffer);
	std::string search_prefix = "auto";
	std::string search = search_prefix + wireless_adapter;
	int location = to_parse.find(search);
	if (location > 0)
	{
		int next_pos = to_parse.find(search_prefix, location + search.size());
		to_parse = to_parse.substr(0, location) + (next_pos >= 0 ? to_parse.substr(next_pos, to_parse.size()) : "");
	}

	return to_parse;
}
void wifi_configurer::setup_adapter()
{
	wireless_config expected_config;
	memset(&expected_config, 0, sizeof(expected_config));
	//memcpy(expected_config.name, wireless_adapter.c_str(), std::min(wireless_adapter.size(), (unsigned) IFNAMSIZ));
	expected_config.essid_len = std::min((unsigned) wireless_network.size(), (unsigned) IW_ESSID_MAX_SIZE);
	memcpy(expected_config.essid, wireless_network.c_str(), expected_config.essid_len);
	expected_config.key_size = std::min((unsigned) wireless_password.size(), (unsigned) IW_ENCODING_TOKEN_MAX);
	memcpy(expected_config.key, wireless_password.c_str(), expected_config.key_size);
	expected_config.essid_on = 1;
	expected_config.has_essid = 1;
	expected_config.has_key = 1;
	expected_config.key_flags = 32769;
	expected_config.has_mode = 1;
	expected_config.mode = 2;

	wireless_config config;
	int socket;

	socket = iw_sockets_open();
	if (iw_get_basic_config(socket, wireless_adapter.c_str(), &config) < 0)
	{
		BOOST_LOG_TRIVIAL(error) << "Could not open basic info for " + wireless_adapter;
		return;
	}

	if (memcmp(&expected_config, &config, sizeof(expected_config)) != 0)
	{
		if (iw_set_basic_config(socket, wireless_adapter.c_str(), &expected_config) < 0)
		{
			BOOST_LOG_TRIVIAL(error) << "Detected differences in " + wireless_adapter + ", but could not set the adapter up.";
		}
		else
		{
			BOOST_LOG_TRIVIAL(info) << "Set-up complete for " + wireless_adapter;
		}
	}
}
void wifi_configurer::continuous_scan(void * context)
{
	wifi_configurer* local_this = (wifi_configurer*) context;
	while(local_this->continue_running)
	{
		if (local_this->current_address.b.essid_len > 0)
		{
			std::ifstream input(network_config);
			std::string removed = local_this->remove_interface_entry(input);
			input.close();

			std::stringstream ss;
			ss << removed << std::endl;
			ss << "auto " + local_this->wireless_adapter << std::endl;
			ss << "iface " + local_this->wireless_adapter << std::endl;
			ss << "wpa-ssid " + local_this->wireless_network << std::endl;
			ss << "wpa-key-mgmt WPA-PSK" << std::endl;
			ss << "wpa-group TKIP CCMP" << std::endl;
			ss << "wpa-psk " + local_this->wireless_password << std::endl;

			std::ofstream output(network_config);
			output << ss;
		}
	}
}

