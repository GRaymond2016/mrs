/*
 * wifi_configurer.hpp
 *
 *  Created on: 11/11/2014
 *      Author: nyson
 */

#include <list>
#include <string>
#include <istream>

#include <iwlib.h>

#ifndef WIFI_CONFIGURER_HPP_
#define WIFI_CONFIGURER_HPP_

class wifi_configurer
{
public:
	wifi_configurer(std::string adapter, std::string network, std::string password);
	void search();
	void poll_connect();
	void shutdown();
	void setup_adapter();

private:
	static void continuous_scan(void * context);
	std::string remove_interface_entry(std::istream &input);

	std::list<wireless_scan> addresses;
	wireless_scan current_address;
	std::string wireless_network;
	std::string wireless_adapter;
	std::string wireless_password;

	bool continue_running;
};


#endif /* WIFI_CONFIGURER_HPP_ */
