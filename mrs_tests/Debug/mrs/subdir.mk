################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
/home/lordnyson/workspace/mrs/configuration.cpp \
/home/lordnyson/workspace/mrs/data_collector.cpp \
/home/lordnyson/workspace/mrs/data_source.cpp \
/home/lordnyson/workspace/mrs/file_data_sink.cpp \
/home/lordnyson/workspace/mrs/gps_data_proxy.cpp \
/home/lordnyson/workspace/mrs/lap_time_data_analyser.cpp \
/home/lordnyson/workspace/mrs/rtsp_controller.cpp \
/home/lordnyson/workspace/mrs/rtsp_data_proxy.cpp \
/home/lordnyson/workspace/mrs/source_wrapper.cpp \
/home/lordnyson/workspace/mrs/spatial_data_proxy.cpp \
/home/lordnyson/workspace/mrs/usb_video_capture.cpp \
/home/lordnyson/workspace/mrs/wifi_configurer.cpp 

OBJS += \
./mrs/configuration.o \
./mrs/data_collector.o \
./mrs/data_source.o \
./mrs/file_data_sink.o \
./mrs/gps_data_proxy.o \
./mrs/lap_time_data_analyser.o \
./mrs/rtsp_controller.o \
./mrs/rtsp_data_proxy.o \
./mrs/source_wrapper.o \
./mrs/spatial_data_proxy.o \
./mrs/usb_video_capture.o \
./mrs/wifi_configurer.o 

CPP_DEPS += \
./mrs/configuration.d \
./mrs/data_collector.d \
./mrs/data_source.d \
./mrs/file_data_sink.d \
./mrs/gps_data_proxy.d \
./mrs/lap_time_data_analyser.d \
./mrs/rtsp_controller.d \
./mrs/rtsp_data_proxy.d \
./mrs/source_wrapper.d \
./mrs/spatial_data_proxy.d \
./mrs/usb_video_capture.d \
./mrs/wifi_configurer.d 


# Each subdirectory must supply rules for building sources it contributes
mrs/configuration.o: /home/lordnyson/workspace/mrs/configuration.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DBOOST_ALL_DYN_LINK -I"/home/lordnyson/workspace/mrs_tests/cute" -I"/home/lordnyson/workspace/mrs" -I/usr/src/phidget -I/usr/local/include/BasicUsageEnvironment -I/usr/local/include/groupsock -I/usr/local/include/liveMedia -I/usr/local/include/UsageEnvironment -I/usr/src/boost_1.55 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

mrs/data_collector.o: /home/lordnyson/workspace/mrs/data_collector.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DBOOST_ALL_DYN_LINK -I"/home/lordnyson/workspace/mrs_tests/cute" -I"/home/lordnyson/workspace/mrs" -I/usr/src/phidget -I/usr/local/include/BasicUsageEnvironment -I/usr/local/include/groupsock -I/usr/local/include/liveMedia -I/usr/local/include/UsageEnvironment -I/usr/src/boost_1.55 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

mrs/data_source.o: /home/lordnyson/workspace/mrs/data_source.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DBOOST_ALL_DYN_LINK -I"/home/lordnyson/workspace/mrs_tests/cute" -I"/home/lordnyson/workspace/mrs" -I/usr/src/phidget -I/usr/local/include/BasicUsageEnvironment -I/usr/local/include/groupsock -I/usr/local/include/liveMedia -I/usr/local/include/UsageEnvironment -I/usr/src/boost_1.55 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

mrs/file_data_sink.o: /home/lordnyson/workspace/mrs/file_data_sink.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DBOOST_ALL_DYN_LINK -I"/home/lordnyson/workspace/mrs_tests/cute" -I"/home/lordnyson/workspace/mrs" -I/usr/src/phidget -I/usr/local/include/BasicUsageEnvironment -I/usr/local/include/groupsock -I/usr/local/include/liveMedia -I/usr/local/include/UsageEnvironment -I/usr/src/boost_1.55 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

mrs/gps_data_proxy.o: /home/lordnyson/workspace/mrs/gps_data_proxy.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DBOOST_ALL_DYN_LINK -I"/home/lordnyson/workspace/mrs_tests/cute" -I"/home/lordnyson/workspace/mrs" -I/usr/src/phidget -I/usr/local/include/BasicUsageEnvironment -I/usr/local/include/groupsock -I/usr/local/include/liveMedia -I/usr/local/include/UsageEnvironment -I/usr/src/boost_1.55 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

mrs/lap_time_data_analyser.o: /home/lordnyson/workspace/mrs/lap_time_data_analyser.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DBOOST_ALL_DYN_LINK -I"/home/lordnyson/workspace/mrs_tests/cute" -I"/home/lordnyson/workspace/mrs" -I/usr/src/phidget -I/usr/local/include/BasicUsageEnvironment -I/usr/local/include/groupsock -I/usr/local/include/liveMedia -I/usr/local/include/UsageEnvironment -I/usr/src/boost_1.55 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

mrs/rtsp_controller.o: /home/lordnyson/workspace/mrs/rtsp_controller.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DBOOST_ALL_DYN_LINK -I"/home/lordnyson/workspace/mrs_tests/cute" -I"/home/lordnyson/workspace/mrs" -I/usr/src/phidget -I/usr/local/include/BasicUsageEnvironment -I/usr/local/include/groupsock -I/usr/local/include/liveMedia -I/usr/local/include/UsageEnvironment -I/usr/src/boost_1.55 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

mrs/rtsp_data_proxy.o: /home/lordnyson/workspace/mrs/rtsp_data_proxy.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DBOOST_ALL_DYN_LINK -I"/home/lordnyson/workspace/mrs_tests/cute" -I"/home/lordnyson/workspace/mrs" -I/usr/src/phidget -I/usr/local/include/BasicUsageEnvironment -I/usr/local/include/groupsock -I/usr/local/include/liveMedia -I/usr/local/include/UsageEnvironment -I/usr/src/boost_1.55 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

mrs/source_wrapper.o: /home/lordnyson/workspace/mrs/source_wrapper.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DBOOST_ALL_DYN_LINK -I"/home/lordnyson/workspace/mrs_tests/cute" -I"/home/lordnyson/workspace/mrs" -I/usr/src/phidget -I/usr/local/include/BasicUsageEnvironment -I/usr/local/include/groupsock -I/usr/local/include/liveMedia -I/usr/local/include/UsageEnvironment -I/usr/src/boost_1.55 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

mrs/spatial_data_proxy.o: /home/lordnyson/workspace/mrs/spatial_data_proxy.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DBOOST_ALL_DYN_LINK -I"/home/lordnyson/workspace/mrs_tests/cute" -I"/home/lordnyson/workspace/mrs" -I/usr/src/phidget -I/usr/local/include/BasicUsageEnvironment -I/usr/local/include/groupsock -I/usr/local/include/liveMedia -I/usr/local/include/UsageEnvironment -I/usr/src/boost_1.55 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

mrs/usb_video_capture.o: /home/lordnyson/workspace/mrs/usb_video_capture.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DBOOST_ALL_DYN_LINK -I"/home/lordnyson/workspace/mrs_tests/cute" -I"/home/lordnyson/workspace/mrs" -I/usr/src/phidget -I/usr/local/include/BasicUsageEnvironment -I/usr/local/include/groupsock -I/usr/local/include/liveMedia -I/usr/local/include/UsageEnvironment -I/usr/src/boost_1.55 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

mrs/wifi_configurer.o: /home/lordnyson/workspace/mrs/wifi_configurer.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DBOOST_ALL_DYN_LINK -I"/home/lordnyson/workspace/mrs_tests/cute" -I"/home/lordnyson/workspace/mrs" -I/usr/src/phidget -I/usr/local/include/BasicUsageEnvironment -I/usr/local/include/groupsock -I/usr/local/include/liveMedia -I/usr/local/include/UsageEnvironment -I/usr/src/boost_1.55 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


