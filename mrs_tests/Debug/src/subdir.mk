################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Test.cpp \
../src/data_collector_test.cpp \
../src/file_data_sink_test.cpp \
../src/gps_timestamp_test.cpp 

OBJS += \
./src/Test.o \
./src/data_collector_test.o \
./src/file_data_sink_test.o \
./src/gps_timestamp_test.o 

CPP_DEPS += \
./src/Test.d \
./src/data_collector_test.d \
./src/file_data_sink_test.d \
./src/gps_timestamp_test.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DBOOST_ALL_DYN_LINK -I"/home/lordnyson/workspace/mrs_tests/cute" -I"/home/lordnyson/workspace/mrs" -I/usr/src/phidget -I/usr/local/include/BasicUsageEnvironment -I/usr/local/include/groupsock -I/usr/local/include/liveMedia -I/usr/local/include/UsageEnvironment -I/usr/src/boost_1.55 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


