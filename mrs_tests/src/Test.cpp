#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"

#include "file_data_sink_test.h"
#include "data_collector_test.h"
#include "gps_timestamp_test.h"

void runUnitTests(int argc, char const *argv[]){
	cute::suite s;
	s.push_back(CUTE(basic_data_sink_test));
	s.push_back(CUTE(advanced_data_sink_test));
	s.push_back(CUTE(basic_data_collector_test));
	s.push_back(CUTE(basic_parsing_test));
	cute::xml_file_opener xmlfile(argc,argv);
	cute::xml_listener<cute::ide_listener<> >  lis(xmlfile.out);
	cute::makeRunner(lis,argc,argv)(s, "AllTests");
}

int main(int argc, char const *argv[]){
    runUnitTests(argc,argv);
    return 0;
}





