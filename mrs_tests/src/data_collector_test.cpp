/*
 * data_collector_test.cpp
 *
 *  Created on: 29 Apr 2015
 *      Author: lordnyson
 */

#include "data_collector_test.h"

#include <fstream>
#include <vector>
#include <iostream>
#include <boost/shared_ptr.hpp>
#include <data_collector.hpp>
#include <data_source.hpp>
#include <data_types.hpp>

#include "cute.h"

std::vector<char> sdata;
class data_sink_validator : public data_sink
{
	unsigned current;
public:
	data_sink_validator() : current(0)
	{}
	virtual ~data_sink_validator()
	{}
	void add_data(unsigned long long timestamp, unsigned long long data_type, boost::shared_ptr< std::vector<char> > &data)
	{
		ASSERTM("data was modified by collector", memcmp(&sdata[current], &(data.get())[0], data->size()));
	}
	void flush()
	{

	}
};

void basic_data_collector_test()
{
	std::ifstream stream("data/SerenityTrailer.mp4");
	data_collector collector(100);
	boost::shared_ptr<data_sink> sink(new data_sink_validator());
	collector.add_data_sink(MRS::data_types::h264_data, sink);
	collector.add_data_sink(MRS::data_types::spatial_data, sink);
	sdata.resize(400 * 400);
	ASSERTM("file was not able to be opened", stream.is_open());
	for (int i = 0; i < 400 && stream.is_open(); ++i)
	{
		boost::shared_ptr<std::vector<char> > to_write(new std::vector<char>(400));
		boost::shared_ptr<data_element> element;
		if (i % 2 == 0)
		{
			unsigned type = MRS::data_types::h264_data + i / 2;
			stream.read(&(*to_write.get())[0], 400);
			element.reset(new data_element(type, to_write));
		}
		else
		{
			unsigned type = MRS::data_types::spatial_data + i / 2;
			memset(&(*to_write.get())[0], 'e', 400);
			element.reset(new data_element(type, to_write));
		}
		memcpy(&sdata[i * 400], &to_write.get()[0], 400);
		collector.add_data(element);
	}
	stream.close();
}


