/*
 * file_data_sink.cpp
 *
 *  Created on: 20 Apr 2015
 *      Author: lordnyson
 */

#include <file_data_sink.hpp>
#include <data_types.hpp>
#include <vector>
#include <fstream>
#include <iostream>

#include <boost/shared_ptr.hpp>

#include "cute.h"

void basic_data_sink_test()
{
	file_data_sink sink("test_data", "file_data_output", ".txt");
	boost::shared_ptr<std::vector<char> > data(new std::vector<char>(400, 'e'));
	sink.add_data(0, MRS::data_types::h264_data, data);
	sink.flush();

	std::ifstream stream("test_data/file_data_output.txt");
	std::ostringstream output;
	std::copy(std::istreambuf_iterator<char>(stream),
				std::istreambuf_iterator<char>(),
				std::ostreambuf_iterator<char>(output));

	std::string outputstr = output.str();
	ASSERTM("File of unexpected size.", outputstr.size() == 00);
	for (unsigned i = 0; i < outputstr.size(); ++i) {
		ASSERTM("File contents not as expected.", outputstr[0] == 'e');
	}

	std::remove("test_data/file_data_output.txt");
}

void advanced_data_sink_test()
{
	file_data_sink sink("test_data", "file_data_output_ad", ".txt");
	boost::shared_ptr<std::vector<char> > data(new std::vector<char>(400, 'e'));
	boost::shared_ptr<std::vector<char> > data1(new std::vector<char>(400, 'z'));
	sink.add_data(0, MRS::data_types::h264_data + 5, data);
	sink.add_data(100, MRS::data_types::spatial_data, data1);

	std::ifstream stream("test_data/file_data_output_ad.txt");
	std::ostringstream output;
	std::copy(std::istreambuf_iterator<char>(stream),
				std::istreambuf_iterator<char>(),
				std::ostreambuf_iterator<char>(output));

	std::string outputstr = output.str();
	ASSERTM("File of unexpected size.", outputstr.size() == 00);
	for (unsigned i = 0; i < outputstr.size(); ++i) {
		ASSERTM("File contents not as expected.", outputstr[0] == 'e');
	}


	std::remove("test_data/file_data_output_ad.txt");
}







