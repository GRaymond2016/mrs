/*
 * file_data_sink_test.h
 *
 *  Created on: 23 Apr 2015
 *      Author: lordnyson
 */

#ifndef FILE_DATA_SINK_TEST_H_
#define FILE_DATA_SINK_TEST_H_

void basic_data_sink_test();
void advanced_data_sink_test();

#endif /* FILE_DATA_SINK_TEST_H_ */
