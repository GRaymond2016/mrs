/*
 * gps_timestamp_test.cpp
 *
 *  Created on: 30 Jun 2015
 *      Author: lordnyson
 */
#include "gps_timestamp_test.h"

#include <sstream>
#include <gps_timestamp.hpp>

#include "cute.h"

void basic_parsing_test()
{
	gps_timestamp timestamp(2015, 5, 28, 12, 14, 50, 576);
	std::stringstream ss;
	ss << timestamp;

	gps_timestamp output;
	ss >> output;

	ASSERTM("Read/write of gps_timestamp year failed.", timestamp.year == output.year);
	ASSERTM("Read/write of gps_timestamp month failed.", timestamp.month == output.month);
	ASSERTM("Read/write of gps_timestamp day failed.", timestamp.day == output.day);
	ASSERTM("Read/write of gps_timestamp hour failed.", timestamp.hour == output.hour);
	ASSERTM("Read/write of gps_timestamp minute failed.", timestamp.minute == output.minute);
	ASSERTM("Read/write of gps_timestamp second failed.", timestamp.second == output.second);
	ASSERTM("Read/write of gps_timestamp millisecond failed.", timestamp.millisecond == output.millisecond);
}
